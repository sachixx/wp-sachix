<?php get_header(); ?>

<?php the_breadcrumb(); ?>
 <div class="contener">
  <div class="maincontener">
  <div class="mainContent cf">
      <main role="main">


        <section id="post-entry">
        <div class="contents">
          <?php if(have_posts()): while(have_posts()):the_post(); ?>
                <div class="title cf">
                  <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                  <time datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('Y.m.d'); ?></time>
                </div>
                <div class="img-area">
            <figure>
              <?php echo get_thumb_img('large', "no-image"); ?>
            </figure>
            <!--   <p class="category-icon <?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->slug; } ?>"><?php the_category(', '); ?></p>  ?-->
                </div>

<div class="tags-area">
<i class="fas fa-tags"></i><?php the_tags( '', ' ・ ' ); ?>
</div>


  <div class="text-area">
    <p><?php the_content('Read more'); ?></p>
  </div>

<?php endwhile; endif; ?>
  
<!--?php previous_post_link('%link','古い記事へ'); ?-->
<!--?php next_post_link('%link','新しい記事へ'); ?-->

</div>
</section>

 

</main>


       <!-- div class="side-area">
          <amp-auto-ads type="adsense"
              data-ad-client="ca-pub-1328044307380418">
</amp-auto-ads>
          </div -->

 <div class="same-category">
<h3><?php the_category(' | '); ?>の最新記事</h3>
<?php
$post_id = get_the_ID(); //投稿IDを取得
foreach((get_the_category()) as $cat) {
$cat_id = $cat->cat_ID ; //カテゴリーIDを取得
break ;
}
query_posts(
array(
'cat' => $cat_id,
'showposts' => 3, //表示件数
'post__not_in' => array($post_id), //post__not_inで取得した投稿IDの記事を除外
)
);
if(have_posts()) :
?>
<ul>
<?php while (have_posts()) : the_post(); ?>
<li>
<a class="cf" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
<!-- サムネイル画像がある場合はサムネイル画像を呼び出す -->
<?php if ( has_post_thumbnail() ): ?>
<?php echo get_the_post_thumbnail($post->ID); ?>
<!-- サムネイル画像がない場合は指定したNO-IMAGE画像を呼び出す -->
<?php else: ?>

<img src="<?php echo get_template_directory_uri(); ?>/img/no-image.png" alt="NO IMAGE" title="NO IMAGE" />

<?php endif; ?>
<p><?php the_title(); ?></p>
</a>
</li>
<?php endwhile; ?>
</ul>
<?php else : ?>
<p>関連記事はありません</p>
<?php endif; ?>
<?php wp_reset_query(); ?>
 </div>
</div>
        </div>
  <div class="sidecontener">
<div class="sideContent">
    <aside>

<div class="ranking-title">ranking</div>

  
<?php
// views post metaで記事のPV情報を取得する
setPostViews(get_the_ID());

// ループ開始
query_posts('meta_key=post_views_count&orderby=meta_value_num&posts_per_page=5&order=DESC'); while(have_posts()) : the_post(); ?>
<div class="ranking cf">
<!-- サムネイルの表示 -->
<div class="ranking-img">
  <a href="<?php echo get_permalink(); ?>">
    <?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'post-thumbnail'); } ?>
  </a>
</div>

<!-- タイトルの表示 -->
<div class="ranking-text">
  <p>
     <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
  </p>
</div>
</div>
<?php endwhile; ?>

    </aside>
  </div>
</div>
</div>
<?php get_footer(); ?>​