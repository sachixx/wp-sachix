<?php get_header(); ?>


<ul id="breadcrumb" class="cf">
    <li>トップ</li>
</ul>

    <!-- Page content -->
<div class="contener">
  <div class="maincontener">
      	 	<div class="_mainContent" id="sec-post">
      	 		<main role="main">
                    <div class="item-box">

<!--カテゴリーIDが「1,3,5」で表示件数が「15記事」の場合-->
<!--?php query_posts('cat=1,3,5&posts_per_page=15'); ?-->
<?php query_posts('posts_per_page=30'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <div class="item">
                <article>
                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                        <figure class="postThumb">
                            <?php the_post_thumbnail('add_img01'); ?>
                        </figure>

                                               <time datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('Y.m.d'); ?></time>

                        <h3 class="postTitle"><?php the_title(); ?></h3>

                        <div class="category-icon <?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->slug; } ?>">
                            <p class="postCategory"><?php the_category(', ') ?></p>
                        </div>

                    </a>
                </article>
               </div>
<?php endwhile; endif; ?>
</div>
 </main>
</div>
</div>
 <div class="sidecontener">
<div class="sideContent">
    <aside>
       

    </aside>
</div>
     </div>       </div>
             </div>
       
        <!--div class="subContent">
            <aside>
                <section>
                    <h2>side-nav</h2>
                </section>
            </aside>
        </div-->
          <!--script>
        $('.slider-box').slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 2,
            slidesToScroll: 1,
            autoplay: false,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows:false
                    }
                }
            ]
        });
        </script-->
<?php get_footer(); ?>
