<?php get_header(); ?>


<?php the_breadcrumb(); ?>

<div class="contener">
  <div class="maincontener">
            <div class="mainContent cf" id="sec-post">
                <main role="main">

   <!--div class="mainContent cf" id=sec-post -->
 <!--div class="contents" -->
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>




 <article>
                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                        <figure class="postThumb">
                            <?php the_post_thumbnail('add_img01'); ?>
                        </figure>

                        <time datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('Y.m.d'); ?></time>
                        <h3 class="postTitle"><?php the_title(); ?></h3>
                        <div class="category-icon <?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->slug; } ?>">
                            <p class="postCategory"><?php the_category(', ') ?></p>
                        </div>
       
                    </a>
                </article>
               


    <?php endwhile;endif; ?>



</main>
            </div>
             </div>

               <div class="sidecontener">
<div class="sideContent">
    <aside>


  <div class="twitter">
    <a class="twitter-timeline" data-height="600" href="https://twitter.com/qrsachi?ref_src=twsrc%5Etfw">Tweets by qrsachi</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  </div>
    </aside>
  </div>
</div>
</div>
<?php get_footer(); ?>
