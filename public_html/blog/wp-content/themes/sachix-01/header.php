<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php my_description(); ?>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link href="<?php echo get_stylesheet_uri(); ?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/blog/wp-content/themes/sachix-01/drawer.min.css">
   <?php wp_head(); ?>
   <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127478848-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127478848-1');
</script>
</head>
<body class="drawer  drawer--top  drawer--navbarTopGutter" >
    <header class="drawer-navbar drawer-navbar--fixed" role="banner">
        <div class="drawer-container container">
            <div class="drawer-navbar-header">
                <!--a class="drawer-brand" href="/">sachix.net</a-->


 <a class="drawer-brand" href="/blog/"><img src="/images/logo.svg" style="width: 6em; padding-top:0.4em;" alt=""></a>

                <button type="button" class="drawer-toggle drawer-hamburger">
                    <span class="sr-only">toggle navigation</span>
                    <span class="drawer-hamburger-icon"></span>
                </button>
            </div>
            <nav class="drawer-nav" role="navigation">
                <ul class="drawer-menu drawer-menu--right">
                    <li><a class="drawer-menu-item" href="/blog/">blog</a></li>
                    <li><a class="drawer-menu-item" href="/works/">works</a></li>
                    <li><a class="drawer-menu-item" href="#">about</a></li>
                </ul>
            </nav>
        </div>
    </header>