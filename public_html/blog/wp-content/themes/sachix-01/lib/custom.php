<?php 
// functions.phpに以下を追記
add_action('admin_menu', 'add_custom_fields');
add_action('save_post', 'save_custom_fields');
 
// 記事ページと固定ページでカスタムフィールドを表示
function add_custom_fields() {
  add_meta_box( 'my_sectionid', 'カスタムフィールド', 'my_custom_fields', 'post');
  add_meta_box( 'my_sectionid', 'カスタムフィールド', 'my_custom_fields', 'page');
}
 
function my_custom_fields() {
  global $post;
  $keywords = get_post_meta($post->ID,'keywords',true);
  $description = get_post_meta($post->ID,'description',true);
   
  echo '<p>キーワード（半角カンマ区切り）<br>';
  echo '<input type="text" name="keywords" value="'.esc_html($keywords).'" size="60" /></p>';
   
  echo '<p>ページの説明（description）160文字以内<br>';
  echo '<input type="text" style="width: 600px;height: 40px;" name="description" value="'.esc_html($description).'" maxlength="160" /></p>';
}
 
// カスタムフィールドの値を保存
function save_custom_fields( $post_id ) {
  if(!empty($_POST['keywords']))
    update_post_meta($post_id, 'keywords', $_POST['keywords'] );
  else delete_post_meta($post_id, 'keywords');
 
  if(!empty($_POST['description']))
    update_post_meta($post_id, 'description', $_POST['description'] );
  else delete_post_meta($post_id, 'description');
}
 
function my_description() {
 
// カスタムフィールドの値を読み込む
$custom = get_post_custom();
if(!empty( $custom['keywords'][0])) {
  $keywords = $custom['keywords'][0];
}
if(!empty( $custom['description'][0])) {
  $description = $custom['description'][0];
}
?>
<?php if(is_home()): // トップページ ?>
<meta name="robots" content="index, follow" />
<meta name="keywords" content="トップページに表示させるkeywords(半角カンマ区切り)">
<meta name="description" content="トップページに表示させるdescription" />
<?php elseif(is_single()): // 記事ページ ?>
<meta name="robots" content="index, follow" />
<meta name="keywords" content="<?php echo $keywords ?>">
<meta name="description" content="<?php echo $description ?>">
<?php elseif(is_page()): // 固定ページ ?>
<meta name="robots" content="index, follow" />
<meta name="keywords" content="<?php echo $keywords ?>">
<meta name="description" content="<?php echo $description ?>">
<?php elseif (is_category()): // カテゴリーページ ?>
<meta name="robots" content="index, follow" />
<meta name="description" content="<?php single_cat_title(); ?>の記事一覧" />
<?php elseif (is_tag()): // タグページ ?>
<meta name="robots" content="noindex, follow" />
<meta name="description" content="<?php single_tag_title("", true); ?>の記事一覧" />
<?php elseif(is_404()): // 404ページ ?>
<meta name="robots" content="noindex, follow" />
<title><?php echo 'お探しのページが見つかりませんでした'; ?></title>
<?php else: // その他ページ ?>
<meta name="robots" content="noindex, follow" />
<?php endif; ?>
<?php
}


// toc mokuji functions.phpに以下を追記
// footer.phpに以下を追記追記に変更
//https://plusers.net/wordpress_toc
/*
function read_toc_scripts(){
  wp_enqueue_script( 'toc', get_template_directory_uri() .'/toc.js', array('jquery') );
}
add_action( 'wp_enqueue_scripts' , 'read_toc_scripts' );
*/
/*自動でソース挿入*/
function toc_in($the_content) {
  if (is_single()) {
    $toc = "<div id=\"toc\"></div>";
 
    $h2 = '/<h2.*?>/i';//H2見出し
    if ( preg_match( $h2, $the_content, $h2s )) {
      $the_content  = preg_replace($h2, $toc.$h2s[0], $the_content, 1);
    }
  }
  return $the_content;
}
add_filter('the_content','toc_in');

//合わせて読みたいショートコード　start
function related_func ( $atts ) {
    extract( shortcode_atts( array(
        'id' => '', 
        'label' => '合わせて読みたい',
    ), $atts ) );
     
    $ids = mb_split(",",$id);
    $outputTag = '';
     
    if($id):
        $outputTag .= '
        <div class="awasete">
        <h5 class="awasete__title">
        <span><i class="fas fa-link"></i>' . $label . '</span>
        </h5>
        <ul class="awasete__list">';
     
        foreach($ids as $value):
            if(ctype_digit($value)):
                $link = get_permalink($value);
                $title = get_the_title($value);
                $date = get_the_time('Y.m.d',$value);
                if(get_post_thumbnail_id($value)){
                    $thmbnail_url = wp_get_attachment_url(get_post_thumbnail_id( $value )); 
                }else{
                    $thmbnail_url = '/wp-content/themes/wdd2/images/common/no-image.jpg';   
                }
                $outputTag .='
                <li class="awasete__list__item">
                    <a class="awasete__list__item__link" href="'. $link . '" target="_blank">
                        <figure class="awasete__list__item__link__image">
                        <img src="' . $thmbnail_url . '">
                        </figure>
                        <div class="awasete__list__item__link__content">
                        <h6 class="awasete__list__item__link__content__title">' . $title . '</h6>
                        <time class="awasete__list__item__link__content__date">' . $date . '<time>
                        </div>
                    </a>
                </li>';
            else:
                $outputTag .='<li class="awasete__list__item">記事IDの指定が正しくありません</li>';
            endif;
        endforeach;
        $outputTag .= '</ul></div>';
        return $outputTag;
    else:
        return '
        <div class="awasete">
        <h5 class="awasete__title">
        <span><i class="fas fa-link"></i>' . $label . '</span>
        </h5>
        <ul class="awasete__list">記事IDがありません</ul>
        </div>';
    endif;
}
add_shortcode('related', 'related_func');
//合わせて読みたいショートコード　end


// 人気記事出力用　start
function getPostViews($postID){
  $count_key = 'post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count==''){
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
      return "0 View";
  }
  return $count.' Views';
}
function setPostViews($postID) {
  $count_key = 'post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count==''){
      $count = 0;
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
  }else{
      $count++;
      update_post_meta($postID, $count_key, $count);
  }
}
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
// 人気記事出力用　end
