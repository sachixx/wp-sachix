
<div class="sideContent">

<div class="sidebar__title">こちらの投稿もおすすめ</div>

        <div class="item-box category">

  <!--カテゴリーIDが「1,3,5」で表示件数が「15記事」の場合-->
  <!--?php query_posts('cat=1,3,5&posts_per_page=15'); ?-->
  <?php query_posts($query_string.'&posts_per_page=4'); ?>
  <!--?php query_posts('posts_per_page=30'); ?-->
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div class="item-box__item">
                  <article>
                      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                          <figure class="postThumb">
                              <?php the_post_thumbnail('add_img01'); ?>
                          </figure>
                          <h3 class="postTitle layout-02"><?php the_title(); ?></h3>

                      </a>
                  </article>
                 </div>
  <?php endwhile; endif; ?>
  </div>


<div class="sidebar__title">過去の投稿</div>
<?php
$year_prev = null;
$postType = get_post_type( );
$months = $wpdb->get_results("SELECT DISTINCT MONTH( post_date ) AS month ,
                                    YEAR( post_date ) AS year,
                                    COUNT( id ) as post_count FROM $wpdb->posts
                                    WHERE post_status = 'publish' and post_date <= now( )
                                    and post_type = '$postType'
                                    GROUP BY month , year
                                    ORDER BY post_date DESC");
foreach($months as $month):
    $year_current = $month->year;
    if ($year_current != $year_prev) { ?>
        <?php if($year_prev != null): ?>
            </ul>
        <?php endif; ?>

        <div class="sidebar__archive-year"><?php echo $month->year; ?>年</div>
        <ul class="sidebar__archive-month">
    <?php } ?>
            <li>
                <a href="<?php echo esc_url(home_url()); ?>/<?php echo $month->year; ?>/<?php echo date("m", mktime(0, 0, 0, $month->month, 1, $month->year)) ?>">
                    <?php echo date("n", mktime(0, 0, 0, $month->month, 1, $month->year)) ?>月(<?php echo $month->post_count; ?>)
                </a>
            </li>
            <?php $year_prev = $year_current; ?>
<?php endforeach; ?>
        </ul>


        </div>