<?php
// https://wemo.tech/
// 統合のトップ画像のサイズ変更
function get_thumb_img($size = 'full', $alt = null) {
  
  $thumb_id = get_post_thumbnail_id();
  
  $thumb_img = wp_get_attachment_image_src($thumb_id, $size);
  
  $thumb_src = $thumb_img[0];
  
  $alt = ($alt) ? $alt : get_the_title();  //追記部分。alt属性に関する引数が空だった場合、デフォルトで投稿タイトルを入力する

  return '<img src="'.$thumb_src.'" alt="'.$alt.'">';
}