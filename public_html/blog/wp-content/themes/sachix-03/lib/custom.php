<?php
//toc 目次自動挿入
/*function toc_in($the_content) {
  if (is_single()) {
    $toc = "<div id=\"toc\"></div>";
 
    $h2 = '/<h2.*?>/i';//H2見出し
    if ( preg_match( $h2, $the_content, $h2s )) {
      $the_content  = preg_replace($h2, $toc.$h2s[0], $the_content, 1);
    }
  }
  return $the_content;
}
add_filter('the_content','toc_in');
*/

//合わせて読みたいショートコード　start
function related_func ( $atts ) {
    extract( shortcode_atts( array(
        'id' => '', 
        'label' => '合わせて読みたい',
    ), $atts ) );
     
    $ids = mb_split(",",$id);
    $outputTag = '';
     
    if($id):
        $outputTag .= '
        <div class="awasete">
        <h5 class="awasete__title">
        <span><i class="fas fa-link"></i>' . $label . '</span>
        </h5>
        <ul class="awasete__list cf">';
     
        foreach($ids as $value):
            if(ctype_digit($value)):
                $link = get_permalink($value);
                $title = get_the_title($value);
                $date = get_the_time('Y.m.d',$value);
                if(get_post_thumbnail_id($value)){
                    $thmbnail_url = wp_get_attachment_url(get_post_thumbnail_id( $value )); 
                }else{
                    $thmbnail_url = '/wp-content/themes/wdd2/images/common/no-image.jpg';   
                }
                $outputTag .='
                <li class="awasete__list__item">
                    <a class="awasete__list__item__link flex flex--bet" href="'. $link . '" target="_blank">
                        <figure class="awasete__list__item__link__image">
                        <img src="' . $thmbnail_url . '">
                        </figure>
                        <div class="awasete__list__item__link__content">
                        <h6 class="awasete__list__item__link__content__title">' . $title . '</h6>
                        <time class="awasete__list__item__link__content__date">' . $date . '<time>
                        </div>
                    </a>
                </li>';
            else:
                $outputTag .='<li class="awasete__list__item">記事IDの指定が正しくありません</li>';
            endif;
        endforeach;
        $outputTag .= '</ul></div>';
        return $outputTag;
    else:
        return '
        <div class="awasete">
        <h5 class="awasete__title">
        <span><i class="fas fa-link"></i>' . $label . '</span>
        </h5>
        <ul class="awasete__list">記事IDがありません</ul>
        </div>';
    endif;
}
add_shortcode('related', 'related_func');
//合わせて読みたいショートコード　end
//記事一覧　archive-title
function my_archive_title($title) {
    if ( is_category() ) {
        $title = single_cat_title( '', false ); 
    } elseif ( is_tag() ) { 
        $title = single_tag_title( '', false ); 
    } 
    $title = $title. 'の記事一覧'; 
    return $title; 
}; 
add_filter( 'get_the_archive_title', 'my_archive_title');
