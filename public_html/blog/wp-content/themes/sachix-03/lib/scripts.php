<?php

function add_files() {
	// WordPress本体のjquery.jsを読み込まない
	wp_deregister_script('jquery');
	wp_dequeue_style('wp-block-library');
 
	// jQueryの読み込み
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', "", "201904", false );

// サイト共通JS
	// wp_enqueue_script( 'smart-script', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), '20160608', true );

// googlepretty
	 //wp_enqueue_script('google-code-prettify-js', get_template_directory_uri() . '/prettify.js', null, "201904", true);
	 //wp_enqueue_script('google-code-prettify-js', 'https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js', null, "201904", false);
 
	// サイト共通のCSSの読み込み
	 wp_enqueue_style( 'main', get_template_directory_uri() . '/style.css', "", '20160608' );
	// wp_enqueue_style( 'drawer','https://cdnjs.cloudflare.com/ajax/libs/drawer/3.2.2/css/drawer.min.css', "", '20160608' );

	// googlepretty css
	// wp_enqueue_style( 'google-code-prettify-css', get_template_directory_uri() . '/prettify.css', "", '20160608' );
	// wp_enqueue_style( 'tomorrow-night-blue-css', get_template_directory_uri() . '/tomorrow-night-blue.min.css', "", '20160608' );
	 // toc
	 wp_enqueue_script('toc-script', get_template_directory_uri() . '/toc.js', null, "201904", true);
	 //
	 wp_enqueue_script( 'webfont-script', '//webfonts.xserver.jp/js/xserver.js', array( 'jquery' ), '20160608', false );
	 	 // gotop
	 wp_enqueue_script('gotop-script', get_template_directory_uri() . '/js/gotop.js', null, "201904", true);
	 // drawer
	// wp_enqueue_script('drawer-script', 'https://cdnjs.cloudflare.com/ajax/libs/drawer/3.2.2/js/drawer.min.js', null, "201904", true);
	
}
add_action( 'wp_enqueue_scripts', 'add_files' );




// headに追加
add_action( 'wp_head', 'add_meta_to_head' );
function add_meta_to_head() {
  //echo '<script async type="text/javascript" src="//webfonts.xserver.jp/js/xserver.js"></script>'
  ;
}



// footerに追加
add_action( 'wp_footer', 'add_meta_to_footer' );
function add_meta_to_footer() {
//  echo '<script>';
//  echo '$(document).ready(function() {';
//  echo '$(".drawer").drawer();';
//  echo '});';
//  echo ' </script>';
}



// singleページ専用JS
//if ( is_single() ) wp_enqueue_script('google-code-prettify-js', 'https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?skin=sons-of-obsidian', null, null, false);

