<?php 
//head内不要ソース削除
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles' );
remove_action('admin_print_styles', 'print_emoji_styles');

//description内のpタグ削除
remove_filter('the_excerpt', 'wpautop');
remove_filter('term_description','wpautop');