<!-- sns-btns -->
<nav class="sns-btns">
  <ul>
    <li><a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-lang="ja" data-show-count="false">Tweet</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></li>
    <li>
<iframe src="https://www.facebook.com/plugins/share_button.php?href=<?php the_permalink(); ?>&layout=button_count&size=small&width=72&height=20&appId" width="72" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe></li>
    <li><div class="line-it-button" data-lang="ja" data-type="share-a" data-ver="2" data-url="<?php the_permalink(); ?>" style="display: none;"></div><script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script></li>
  </ul>
</nav><!-- /sns-btns -->