<?php get_header(); ?>

 <div class="contener post-entry-contener">
  <div class="maincontener">

      <main>

<?php the_archive_title( '<h1 class="page-title">', '</h1>' );?>


        <div class="item-box category">

  <!--カテゴリーIDが「1,3,5」で表示件数が「15記事」の場合-->
  <!--?php query_posts('cat=1,3,5&posts_per_page=15'); ?-->
  <?php query_posts($query_string.'&posts_per_page=30'); ?>
  <!--?php query_posts('posts_per_page=30'); ?-->
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div class="item-box__item">
                  <article>
                      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                          <figure class="postThumb">
                              <?php the_post_thumbnail('add_img01'); ?>
                          </figure>

                          <time class="layout-02" datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('Y.m.d'); ?></time>

                          <h3 class="postTitle layout-02"><?php the_title(); ?></h3>

  <div class="postcategory <?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->slug; } ?>">
                              <p><?php the_category('') ?></p>
                          </div>

                      </a>
                  </article>
                 </div>
  <?php endwhile; endif; ?>
  </div>

 

</main>

        </div>
  <div class="sidecontener">
  <?php get_sidebar(); ?>
  </div>
</div>
<?php get_footer(); ?>​