<?php get_header(); ?>

 <div class="contener post-entry-contener">
  <div class="maincontener">

      <main>
<!--?php the_breadcrumb(); ?-->

        <section>
        <div id="post-entry">
          <?php if(have_posts()): while(have_posts()):the_post(); ?>


      <div class="post-entry__title layout-01">
      <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
      <time datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('Y.m.d'); ?></time>

    </div>

                <div class="post-entry__img-area">
            <figure>
              <?php echo get_thumb_img('large', "no-image"); ?>
            </figure>

              <div class="postcategory post-entry__category-icon <?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->slug; } ?>">
                              <p><?php the_category('') ?></p>
                          </div>
                </div>


<div class="img-area-under layout-01">
    <dl class="lead">
   <dt><?php echo nl2br(get_post_meta($post->ID, 'lead_name', true)); ?></dt>
    <dd><?php
  $values = explode("\n" , get_post_meta($post->ID, 'lead_text', true));
  foreach( $values as $value ) {
    echo "<p>" .$value. "</p>\n";
  }
?></dd>
    </dl>

<div class="lead-sub">
   <div class="keyword">
      <p>この記事のキーワード</p>
        <?php the_tags( '<ul><li>', '</li><li>', '</li></ul>' ); ?>
   </div>

</div>
</div>
<!--目次-->
  <div id='toc'></div>
<!--テキストエリア-->
  <div class="post-entry__text-area">
    <p><?php the_content('Read more'); ?></p>
  </div>

<?php endwhile; endif; ?>
  
<!--?php previous_post_link('%link','古い記事へ'); ?-->
<!--?php next_post_link('%link','新しい記事へ'); ?-->

</div>
</section>

 

</main>

        </div>
  <div class="sidecontener">
  <?php get_sidebar(); ?>
  </div>
</div>
<?php get_footer(); ?>​